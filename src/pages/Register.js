import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {
	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [firstname, setFirstname] = useState('');
	const [lastname, setLastname] = useState('');
	const [mobile, setMobile] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine wheter the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// User info
	const {user, setUser} = useContext(UserContext);

	console.log(email);
	console.log(password1);
	console.log(password2);

	function registerUser(e){
		e.preventDefault();


		/*setUser({
			firstName: firstname,
			lastName: lastname,
			email: email,
			password: password1,
			mobileNo: mobile
		})*/

		fetch('http://localhost:4000/users/register', {
		    method: 'POST',
		    headers: {
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		        firstName: firstname,
		        lastName: lastname,
		        email: email,
		        password: password1,
		        mobileNo: mobile
		    })
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
			    title: "Registration Successful",
			    icon: "success",
			    text: "Welcome to Zuitt!"
			})
		})
	}

	const checkEmail = (token) => {
	    fetch('http://localhost:4000/users/details', {
	        headers: {
	            Authorization: `Bearer ${ token }`
	        }
	    })
	    .then(res => res.json())
	    .then.then(data => this.setState({ data }));
	}

	useEffect(() => {
		// Validate to enable submit button when all fields are populated and both passwords match
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2))
		{
			setIsActive(true);
		}
		else {
			setIsActive(false);
		}
	}, [email, password1, password2])

	return (
		(user.id !== null) ?
            <Redirect to ="/courses"/>
        :
        	<Form onSubmit={(e) => registerUser(e)}>
        	{/* Bind the input states via 2-way binding */}
        		<Form.Group className="mb-3" controlId="userFirstName">
        	    	<Form.Label>First Name</Form.Label>
        	    	<Form.Control 
        	    		type="text" 
        	    		placeholder="Enter First Name" 
        	    		value={firstname}
        	    		onChange={ e => setFirstname(e.target.value)}
        	    		required
        	    	/>
        	  	</Form.Group>

        	  	<Form.Group className="mb-3" controlId="userLastName">
        	    	<Form.Label>Last Name</Form.Label>
        	    	<Form.Control 
        	    		type="text" 
        	    		placeholder="Enter Last Name" 
        	    		value={lastname}
        	    		onChange={ e => setLastname(e.target.value)}
        	    		required
        	    	/>
        	  	</Form.Group>

        		<Form.Group className="mb-3" controlId="userEmail">
        	    	<Form.Label>Email address</Form.Label>
        	    	<Form.Control 
        	    		type="email" 
        	    		placeholder="Enter email" 
        	    		value={email}
        	    		onChange={ e => setEmail(e.target.value)}
        	    		required
        	    	/>
        	    	<Form.Text className="text-muted">
        	      		We'll never share your email with anyone else.
        	    	</Form.Text>
        	  	</Form.Group>

        	  	<Form.Group className="mb-3" controlId="userMobile">
        	    	<Form.Label>Mobile Number</Form.Label>
        	    	<Form.Control 
        	    		type="text" 
        	    		placeholder="Enter Mobile Number" 
        	    		value={mobile}
        	    		onChange={ e => setMobile(e.target.value)}
        	    		required
        	    	/>
        	  	</Form.Group>

        	  	<Form.Group className="mb-3" controlId="password1">
        	    	<Form.Label>Password</Form.Label>
        	    	<Form.Control 
        	    		type="password" 
        	    		placeholder="Password"
        	    		value={password1}
        	    		onChange={ e => setPassword1(e.target.value)}
        	    		required 
        	    	/>
        	  	</Form.Group>

        	  	<Form.Group className="mb-3" controlId="password2">
        	  		<Form.Label>Verify Password</Form.Label>
        	    	<Form.Control 
        	    		type="password"  
        	    		placeholder="Verify Password" 
        	    		value={password2}
        	    		onChange={ e => setPassword2(e.target.value)}
        	    		required 
        	    	/>
        	  	</Form.Group>

        	  	{/* Conditionally render the submit button based on isActive state */}
        	  	{ isActive ? 
        	  		<Button className="mb-3" variant="primary" type="submit" id="submitBtn">
        	  		  	Submit
        	  		</Button>
        	  		:
        	  		// Disabled state
        	  		<Button variant="danger" type="submit" id="submitBtn" disabled>
        	  		  	Submit
        	  		</Button>
        	  	}
        	</Form>
	)
}